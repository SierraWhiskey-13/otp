# One Time Pad (Pass) Generator

This series of scripts will generate One Time Pads as needed.

The original article can be found at: (https://www.americanpartisan.org/2020/01/r-pi-otp-dryad-true-hardware-rng-how-to/).

Tested on a RPI 4B running RaspbianOS Bullseye (2021-10-30-armhf).

You will need `rng-tools` installed to access the hardware RNG generator (optional, but recommended).  
`sudo apt install rng-tools`

### Script Details
- __otp.sh__ 
  - This script needs to run with _sudo_ to access the RAMDisk functions (`sudo ./otp.sh`).
  - This script will clear the clipboard on exit so make sure you have pasted your table before exiting.

