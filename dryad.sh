#!/bin/bash
#
# DRYAD Authentication Table Generator
#

# Must be run as root to create a RAM disk.
if [ "$EUID" -ne 0 ]
    then 
		    echo "Must run as root"
				echo "Usage: sudo $0"
		exit
fi

# Where to generate the DRYAD Tables - putting it in a ramdisk
dryaddir="/mnt/tmp"
dryadfile="$dryaddir/dryad.txt"

# Create a ramdisk to put the table in to keep it in memory and not write to SD card
# mkfs -q /dev/ram1 1024
# mkdir -p /ramdisk
# mount /dev/ram1 /ramdisk
[[ -d "/mnt/tmp" ]] && umount /mnt/tmp && rm -rf /mnt/tmp
mkdir -p /mnt/tmp
mount -t tmpfs -o size=1g tmpfs /mnt/tmp
touch $dryadfile

# This determines how many Tables to print
read -p "How many DRYAD Tables to generate? " tables

# Generate the Tables
for ((k=1; k<=$tables; k++))
do
    rows='A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'
    echo "FOR OFFICIAL USE ONLY     UID:" >> $dryadfile
    echo "" >> $dryadfile
    echo "Start DTG:                End DTG:" >> $dryadfile
    echo "Distribution:" >> $dryadfile
    echo "" >> $dryadfile
    for row in $rows
    do
        if [ $row == 'A' ] || [ $row == 'F' ] || [ $row == 'K' ] || [ $row == 'P' ] || [ $row == 'U' ]
        then
        echo "" >> $dryadfile
        echo "   ABCD   EFG   HIJ   KL   MN   OPQ   RS   TUV   WX   YZ" >> $dryadfile
            echo "     0     1     2    3    4     5    6     7    8    9" >> $dryadfile
        fi
        echo -n $row >> $dryadfile
        dryad=$( echo 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' | sed 's/./&\n/g' | shuf --random-source=/dev/hwrng  | tr -d "\n" )
        A="${dryad:0:4}'   '${dryad:4:3}'   '${dryad:7:3}'   '${dryad:10:2}'   '${dryad:12:2}'   '${dryad:14:3}'   '${dryad:17:2}'   '${dryad:19:3}'   '${dryad:22:2}'   '${dryad:24:2}"
        echo " '"$A"'" >> $dryadfile
    done
    echo "" >> $dryadfile
    echo "" >> $dryadfile
done

# Send the Tables to X's clipboard
xclip -i $dryadfile -sel clip
echo
echo "The DRYAD Tables are now in X's clipboard."
echo

# Prepare to erase the Tables from the ramdisk
read -p "Press (return) to delete the tables from the ramdisk and clear your clipboard..." dumpit

trap "echo -n 'Cleaning up...'
        rm -f $dryadfile && \
	printf '' | xclip -sel clipboard && \
	umount -f $dryaddir && sleep 1 && \
	rm -rf $dryaddir
        echo ' Done.' && echo" \
	EXIT
echo