#!/bin/bash
#
# One Time Pad Table Generator Programs
#

# Must be run as root to create a RAM disk.
if [ "$EUID" -ne 0 ]
    then 
		echo
        echo "Must run as root"
		echo "Usage: sudo $0"
        echo
		exit
fi

# This determines how many characters per group in the OTPs
blocksize=5

# This determines how many groups per line of the OTPs
blockrow=5

# This determines how many OTPs across the page to print
read -p "How many pads across the paper? " tablerow

# This determines how many lines of characters in each OTP
rowcount=10

# This determines how many rows of OTPs to print down the page
read -p "How many rows of pads? " pagecount

# Numbers or letters in the OTP?
read -p "OTP of numbers 0-9 (n) or letters A-Z (l)? default = n " NorL
if [ "$NorL" == "l" ]
then
    TYPE="A-Z"
else
    TYPE="0-9"
    read -p "Arabic numerals (a) or symbols [O\|/+A>V<X] (s)?" AorS
fi

# Have user pick algorithm to use for picking numbers or letters
read -p "Which RNG Device? random-(1), urandom-(2) or hwrng-(3). Default is 1 " DEVICE
case $DEVICE in
    3)
      RNGAlg=hwrng
      ;;
   2)
      RNGAlg=urandom
      ;;
   *)
      RNGAlg=random
      ;;
esac

# Where to generate the page of OTPs - putting it in a ramdisk
otpdir="/mnt/tmp"
otpfile="$otpdir/otp.txt"

# Create a ramdisk to put the tables in to keep them in memory and not write to SD card
# mkfs -q /dev/ram1 1024
# mkdir -p /ramdisk
# mount /dev/ram1 /ramdisk
[[ -d "/mnt/tmp" ]] && umount /mnt/tmp && rm -rf /mnt/tmp
mkdir -p /mnt/tmp
mount -t tmpfs -o size=1g tmpfs /mnt/tmp
touch $otpfile

echo
echo "$RNGAlg it is..."
echo
echo "Generating a Table of $tablerow by $pagecount of ($TYPE - type) OTPs."
echo
echo "                 grinding away…"
echo
# Reset BASH time counter
SECONDS=0
# Generate a row of OTPs
for ((x=1; x<=$pagecount; x++))
do
# Generate a full line of characters for eact OTP in the current row of OTPs
#  echo "" >> $otpath;
    for ((i=1; i<=$rowcount; i++))
    do
        # Generate a row of groups for the current OTP in the current row of OTPs
        for ((k=1; k<=$tablerow; k++))
        do
            # Generate the groups for the current line of groups in the current OTP
            for ((j=1; j<=$blockrow; j++))
            do
                # Generate the current group of characters using the selected atributes
                #    NOTE: using this if-then-else here instead of earlier in the script with two
                #    longer generating branches results in an extra 2-seconds per OTP to generate with
                #    hwrng, while reducing script length
                if [ "$NorL" == "l" ]
                then
                    randnum=$(base32 /dev/$RNGAlg | tr -dc $TYPE | head -c $blocksize)
                else
                    randnum=$(xxd -p /dev/$RNGAlg | tr -dc [:digit:] | head -c $blocksize)
                    if [ "$AorS" == "s" ]
                    then
                        randnum="$(echo $randnum | tr '0123456789' 'O\\|/+A>V<X')"
                        # the first '\'' is required to make the second '\' display - it is otherwise a special meaning character
                    fi
                fi
                echo -n $randnum >> $otpfile
                echo -n " " >> $otpfile
            done
            echo -n "   " >> $otpfile
        done
      echo "" >> $otpfile
    done
    echo "" >> $otpfile
    echo "" >> $otpfile
done
# Send the table to X's clipboard
xclip -i $otpfile -sel clip
echo
echo "The OTP table is now in X's clipboard."
echo
ELAPSED="$(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
echo "Time taken to generate the OTP Table was $ELAPSED"
echo
# Prepare to erase the table from the ramdisk
read -p "Press (return) to delete the OTP table from the ramdisk and clear your clipboard..." dumpit

trap "echo -n 'Cleaning up...'
        rm -f $otpfile && \
	printf '' | xclip -sel clipboard && \
	umount -f $otpdir && sleep 1 && \
	rm -rf $otpdir
        echo ' Done.' && echo" \
	EXIT
echo
